# bitbucket django code coverage

This is an example project to integrate code coverage report and junit test reports using pytest in bitbucket-pipeline.

## Getting started

```bash
conda create -n bitbucket-django-code-coverage python=3.9 -y
source activate bitbucket-django-code-coverage
pip -r requirements.txt
```

## Run pytest with coverage

```bash
pytest . --cov=. --cov-report=html:test-reports/htmlcov --cov-report=xml:test-reports/coverage.xml --cov-report=term --cov-report=annotate --junitxml=test-reports/junit.xml
```

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)
