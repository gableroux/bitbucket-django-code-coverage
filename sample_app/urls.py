# create urls for the view
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
]