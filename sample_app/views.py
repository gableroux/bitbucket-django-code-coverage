from django.shortcuts import render

# Create your views here. Make a view that returns 'Hello World'
def index(request):
    return render(request, 'index.html')
