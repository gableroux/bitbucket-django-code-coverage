from django.test import TestCase
from django.urls import reverse

class TestAdmin(TestCase):
    def test_index_view(self):
        with self.subTest(msg='Test unauthenticated index view redirects to login'):
            response = self.client.get(reverse('admin:index'))
            self.assertEqual(response.status_code, 302, response)
            self.assertEqual(response.url, '/admin/login/?next=/admin/', response)

        with self.subTest(msg='Test admin login view'):
            response = self.client.get(reverse('admin:login'))
            self.assertEqual(response.status_code, 200, response)
            self.assertContains(response, 'Log in')